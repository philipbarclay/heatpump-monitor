// Copyright (c) 2017 Philip Barclay
// Released under MIT license, see LICENSE.txt for details

// #include <SoftwareSerial.h>
#include <Wire.h>
#include <SPI.h>  // Needed by the BME280 library but unused
#include <HeatPump.h>
#include <Homie.h>
#include <SparkFunBME280.h>
#include <DallasTemperature.h>

#include "RemoteDebug.h"

// Hardware connections
// #define DEBUGTX      D6  // GPIO 12
// #define DEBUGRX      D5  // GPIO 14

#define FW_NAME              "heatpumpMon"
#define FW_VERSION           "0.0.1"

#define LED_PIN      D4  // GPIO 2
#define SDA_PIN      D2  // GPIO 4
#define SCL_PIN      D1  // GPIO 5
#define ONEWIRE_PIN  D5  // GPIO 14
ADC_MODE(ADC_VCC);

#define TEMPHUMPRES_I2C_ADDR 0x76

#define DEFAULT_TX_INTERVAL  (10)
//  #define DEFAULT_TX_INTERVAL   (60*5)

RemoteDebug Debug;
String helpCmd;

BME280 bme280;

OneWire oneWire(ONEWIRE_PIN);
DallasTemperature dallasTempDevices(&oneWire);
DeviceAddress extTemperatureAddress;

HeatPump heatpump;

heatpumpSettings previousSettings;
heatpumpStatus previousStatus;

HomieNode roomTempNode("temperature", "temperature");
HomieNode roomHumidityNode("humidity", "humidity");
HomieNode roomPressureNode("pressure", "pressure");

HomieNode heatpumpNode("heatpump", "heatpump");

// HomieNode hpSettingsNode("settings", "json");
// HomieNode hpStatusNode("status", "json");
// HomieNode hpTimersNode("timers", "json");

HomieSetting<long> transmitIntervalSetting("txInterval",
  "The measurement interval of scheduled sensor readings in seconds");

unsigned long lastScheduledDataSent = 0;

bool ambientAirSensor = false;
bool hpExhaustAirSensor = false;

// Telnet debug callback prototypes
void processCmdRemoteDebug();

// Heatpump callback prototypes
void hpSettingsChanged();
void hpStatusChanged(heatpumpStatus currentStatus);
void hpPacketDebug(byte* packet, unsigned int length, char* packetDirection);

void homieSetupHandler()
{
  // hpSetTempNode.setProperty("unit").send("c");
  if (ambientAirSensor)
  {
    roomTempNode.setProperty("unit").send("C");
    roomHumidityNode.setProperty("unit").send("%");
    roomPressureNode.setProperty("unit").send("Pa");
  }
}

void homieLoopHandler()
{
  // This loop takes care of sending the scheduled items like the air
  // temperature.
  if (millis() - lastScheduledDataSent >= transmitIntervalSetting.get() * 1000UL
    || lastScheduledDataSent == 0)
  {
    if (Debug.isActive(Debug.INFO)) {
      Debug.println(F("Scheduled sampling of sensors"));
    }

    // Generate an LED pulse while sampling sensors
    // digitalWrite(LED_PIN, LOW);

    if (ambientAirSensor)
    {
      float t = bme280.readTempC();
      roomTempNode.setProperty("degrees").send(String(t, 1));

      float h = bme280.readFloatHumidity();
      roomHumidityNode.setProperty("percentage").send(String(h, 1));

      float p = bme280.readFloatPressure();
      roomPressureNode.setProperty("pascals").send(String(p, 0));

      if (Debug.isActive(Debug.INFO))
      {
        Debug.print(F("  Ambient temperature: "));
        Debug.print(t);
        Debug.println(F(" °C"));
        Debug.print(F("  Ambient humidity: "));
        Debug.print(h);
        Debug.println(F(" %"));
        Debug.print(F("  Ambient pressure: "));
        Debug.print(p / 1000.0);
        Debug.println(F(" kPa"));
      }
    }

    if (hpExhaustAirSensor)
    {
      dallasTempDevices.requestTemperatures();
      float t = dallasTempDevices.getTempCByIndex(0);
      heatpumpNode.setProperty("exhaustTemperature").send(String(t, 1));

      if (Debug.isActive(Debug.INFO))
      {
        Debug.print(F("  Exhaust temperature: "));
        Debug.print(t);
        Debug.println(F(" °C"));
      }
    }

    lastScheduledDataSent += transmitIntervalSetting.get() * 1000UL;
    // digitalWrite(LED_PIN, HIGH);
  }
}

bool heatpumpSettingsHandler(const HomieRange& range, const String& value)
{
}

void onHomieEvent(const HomieEvent& event)
{
  switch (event.type)
  {
    case HomieEventType::STANDALONE_MODE:
      break;
    case HomieEventType::CONFIGURATION_MODE:
      break;
    case HomieEventType::NORMAL_MODE:
      break;
    case HomieEventType::OTA_STARTED:
      break;
    case HomieEventType::OTA_FAILED:
      break;
    case HomieEventType::OTA_SUCCESSFUL:
      break;
    case HomieEventType::ABOUT_TO_RESET:
      break;
    case HomieEventType::WIFI_CONNECTED:
      MDNS.addService("telnet", "tcp", 23);
      Debug.begin("");
      Debug.setResetCmdEnabled(true);
      Debug.showTime(true);
      Debug.showColors(true);

      // Add custom commands, handled via the callback
      helpCmd = "info - General information\n";
      helpCmd.concat("air - Air measurement");
      Debug.setHelpProjectsCmds(helpCmd);
      Debug.setCallBackProjectCmds(&processCmdRemoteDebug);

      break;
    case HomieEventType::WIFI_DISCONNECTED:
      break;
    case HomieEventType::MQTT_READY:
      if (Debug.isActive(Debug.DEBUG))
      {
        Debug.println(F("HomieEvent: MQTT_CONNECTED"));
      }
      break;
    case HomieEventType::MQTT_DISCONNECTED:
      if (Debug.isActive(Debug.DEBUG))
      {
        Debug.println(F("HomieEvent: MQTT_DISCONNECTED"));
      }
      break;
    case HomieEventType::MQTT_PACKET_ACKNOWLEDGED:
      break;
    case HomieEventType::READY_TO_SLEEP:
      break;
  }
}

void processCmdRemoteDebug()
{
  String lastCmd = Debug.getLastCommand();

  if (lastCmd == "info")
  {
    if (Debug.isActive(Debug.VERBOSE))
    {
      Debug.println(F("Info requested"));
      Debug.println();
      Debug.println(F("HEATPUMP MONITOR"));
      Debug.println(F("Philip Barclay"));
      Debug.println();
      Debug.println(F("ESP8266 Info:"));
      Debug.print(F("  Chip ID: "));
      Debug.println(ESP.getChipId(), HEX);
      Debug.print(F("  Reset Reason: "));
      Debug.println(ESP.getResetReason());
      Debug.print(F("  Flash chip ID: "));
      Debug.println(ESP.getFlashChipId(), HEX);
      Debug.print(F("  Flash chip real size (bytes): "));
      Debug.println(ESP.getFlashChipRealSize());
      Debug.print(F("  Flash chip apparent size (bytes): "));
      Debug.println(ESP.getFlashChipSize());
      Debug.print(F("  Flash chip speed (Hz): "));
      Debug.println(ESP.getFlashChipSpeed() / 1e6);
    }
  }
  else if (lastCmd == "air")
  {
    if (Debug.isActive(Debug.VERBOSE))
    {
      Debug.println(F("Air measurements requested"));
      if (ambientAirSensor)
      {
        Debug.print(F("  Ambient temperature: "));
        Debug.print(bme280.readTempC());
        Debug.println(F(" °C"));
        Debug.print(F("  Ambient humidity: "));
        Debug.print(bme280.readFloatHumidity());
        Debug.println(F(" %"));
        Debug.print(F("  Ambient pressure: "));
        Debug.print(bme280.readFloatPressure() / 1000.0);
        Debug.println(F(" kPa"));
      }
      else
      {
        Debug.print(F("  No BME280 sensor detected"));
      }

      if (hpExhaustAirSensor)
      {
        Debug.print(F("  Heatpump exhaust temperature: "));
        dallasTempDevices.requestTemperatures();
        float temp = dallasTempDevices.getTempCByIndex(0);
        Serial.print(temp);
        Serial.println(F(" °C"));
      }
      else
      {
        Debug.print(F("  No DS18B20 sensor detected"));
      }
    }
  }
}

void setup()
{
  Wire.begin(SDA_PIN, SCL_PIN);  // SDA=D2=GPIO4 SCL=D1=GPIO5

  // Configure BME280 sensor
  bme280.settings.commInterface = I2C_MODE;
  bme280.settings.I2CAddress = TEMPHUMPRES_I2C_ADDR;

  // runMode can be:
  //  0, Sleep mode
  //  1 or 2, Forced mode
  //  3, Normal mode
  bme280.settings.runMode = 3;

  // tStandby can be:
  //  0, 0.5ms
  //  1, 62.5ms
  //  2, 125ms
  //  3, 250ms
  //  4, 500ms
  //  5, 1000ms
  //  6, 10ms
  //  7, 20ms
  bme280.settings.tStandby = 0;

  // filter can be off or number of FIR coefficients to use:
  //  0, filter off
  //  1, coefficients = 2
  //  2, coefficients = 4
  //  3, coefficients = 8
  //  4, coefficients = 16
  bme280.settings.filter = 0;

  // tempOverSample, pressOverSample, humidOverSample can be:
  //  0, skipped
  //  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
  bme280.settings.tempOverSample = 1;
  bme280.settings.pressOverSample = 1;
  bme280.settings.humidOverSample = 1;

  uint8_t id = bme280.begin();
  if (id == 0x60)
  {
    ambientAirSensor = true;
  }

  // Search for optional single external 1-wire temperature sensor.
  dallasTempDevices.begin();
  int extDevCount = dallasTempDevices.getDeviceCount();
  if (extDevCount > 0)
  {
    hpExhaustAirSensor = true;
    dallasTempDevices.getAddress(extTemperatureAddress, 0);
  }

  // Serial0 UART is used for comms with the heatpump
  heatpump.setSettingsChangedCallback(hpSettingsChanged);
  heatpump.setStatusChangedCallback(hpStatusChanged);
  heatpump.setPacketCallback(hpPacketDebug);

  // Using HW serial swapped to alternative pins
  heatpump.serial(&Serial);
  Serial.swap();
  heatpump.connect(NULL);

  Homie_setFirmware(FW_NAME, FW_VERSION);

  Homie.setSetupFunction(homieSetupHandler);
  Homie.setLoopFunction(homieLoopHandler);
  Homie.onEvent(onHomieEvent);
  Homie.disableResetTrigger();
  Homie.setLedPin(LED_PIN, LOW);

  roomTempNode.advertise("units");
  roomTempNode.advertise("degrees");

  roomHumidityNode.advertise("units");
  roomHumidityNode.advertise("percentage");

  roomPressureNode.advertise("units");
  roomPressureNode.advertise("pascals");

  heatpumpNode.advertise("settings");  // .settable(heatpumpSettingsHandler);
  heatpumpNode.advertise("status");    // .settable(heatpumpSettingsHandler);
  heatpumpNode.advertise("timers");    // .settable(heatpumpSettingsHandler);

  heatpumpNode.advertise("power");
  heatpumpNode.advertise("mode");
  heatpumpNode.advertise("temperature");
  heatpumpNode.advertise("fan");
  heatpumpNode.advertise("vane");
  heatpumpNode.advertise("wideVane");
  heatpumpNode.advertise("iSee");

  heatpumpNode.advertise("roomTemperature");
  heatpumpNode.advertise("operating");

  heatpumpNode.advertise("timers");
  heatpumpNode.advertise("timerMode");
  heatpumpNode.advertise("timerOnMins");
  heatpumpNode.advertise("timerOnMinsRemaining");
  heatpumpNode.advertise("timerOffMins");
  heatpumpNode.advertise("timerOffMinsRemaining");

  heatpumpNode.advertise("exhaustTemperature");

  // hpSettingsNode.advertise("json");
  // hpStatusNode.advertise("json");
  // hpTimersNode.advertise("json");

  transmitIntervalSetting.setDefaultValue(DEFAULT_TX_INTERVAL)
    .setValidator([] (long candidate)
  {
    return candidate > 0;
  });

  Homie.setup();

  // Seed things just in case
  lastScheduledDataSent = millis();
}

void sendDebug(uint8_t debugLevel, String str)
{
  if (Debug.isActive(debugLevel))
  {
    Debug.println(str);
  }
}

void loop()
{
  heatpump.sync();
  Homie.loop();
  Debug.handle();
}

void hpSettingsChanged()
{
  if (Debug.isActive(Debug.DEBUG))
  {
    Debug.println(F("hpSettingsChanged callback called"));
  }

  heatpumpSettings currentSettings = heatpump.getSettings();

  /*
  struct heatpumpSettings {
    String power;
    String mode;
    float temperature;
    String fan;
    String vane; //vertical vane, up/down
    String wideVane; //horizontal vane, left/right
    bool iSee;   //iSee sensor, at the moment can only detect it, not set it
    bool connected;
  };
  */

  const size_t bufferSize = JSON_OBJECT_SIZE(7);
  DynamicJsonBuffer jsonBuffer(bufferSize);
  JsonObject& root = jsonBuffer.createObject();

  root["power"]       = currentSettings.power;
  root["mode"]        = currentSettings.mode;
  root["temperature"] = currentSettings.temperature;
  root["fan"]         = currentSettings.fan;
  root["vane"]        = currentSettings.vane;
  root["wideVane"]    = currentSettings.wideVane;
  root["iSee"]        = currentSettings.iSee;

  char buffer[512];
  root.printTo(buffer, sizeof(buffer));

  heatpumpNode.setProperty("settings").send(buffer);
  sendDebug(Debug.INFO, buffer);

  if (currentSettings.power != previousSettings.power)
  {
    heatpumpNode.setProperty("power").send(currentSettings.power);
    sendDebug(Debug.INFO, "  Power: " + currentSettings.power);
  }

  if (currentSettings.mode != previousSettings.mode)
  {
    heatpumpNode.setProperty("mode").send(currentSettings.mode);
    sendDebug(Debug.INFO, "  Mode: " + currentSettings.mode);
  }

  if (currentSettings.temperature != previousSettings.temperature)
  {
    heatpumpNode.setProperty("temperature").send(String(currentSettings.temperature));
    sendDebug(Debug.INFO, "  Temperature: " + String(currentSettings.temperature));
  }

  if (currentSettings.fan != previousSettings.fan)
  {
    heatpumpNode.setProperty("fan").send(currentSettings.fan);
    sendDebug(Debug.INFO, "  Fan: " + currentSettings.fan);
  }

  if (currentSettings.vane != previousSettings.vane)
  {
    heatpumpNode.setProperty("vane").send(currentSettings.vane);
    sendDebug(Debug.INFO, "  Vane: " + currentSettings.vane);
  }

  if (currentSettings.wideVane != previousSettings.wideVane)
  {
    heatpumpNode.setProperty("wideVane").send(currentSettings.wideVane);
    sendDebug(Debug.INFO, "  Wide Vane: " + currentSettings.wideVane);
  }

  if (currentSettings.iSee != previousSettings.iSee)
  {
    heatpumpNode.setProperty("iSee").send(currentSettings.iSee ? "Present" : "Not Present");
    sendDebug(Debug.INFO, "  iSee: " + currentSettings.iSee);
  }

  previousSettings = currentSettings;
}

void hpStatusChanged(heatpumpStatus currentStatus)
{
  if (Debug.isActive(Debug.DEBUG))
  {
    Debug.println(F("hpStatusChanged callback called"));
  }

  /*
  struct heatpumpStatus {
    float roomTemperature;
    bool operating; // if true, the heatpump is operating to reach the desired temperature
    heatpumpTimers timers;
  };
  struct heatpumpTimers {
    String mode;
    int onMinutesSet;
    int onMinutesRemaining;
    int offMinutesSet;
    int offMinutesRemaining;
  };
  */

  // send room temp and operating info
  const size_t bufferSizeInfo = JSON_OBJECT_SIZE(2);
  DynamicJsonBuffer jsonBufferInfo(bufferSizeInfo);

  JsonObject& rootInfo = jsonBufferInfo.createObject();
  rootInfo["roomTemperature"] = currentStatus.roomTemperature;
  rootInfo["operating"]       = currentStatus.operating;

  char bufferInfo[512];
  rootInfo.printTo(bufferInfo, sizeof(bufferInfo));

  heatpumpNode.setProperty("status").send(bufferInfo);
  sendDebug(Debug.INFO, bufferInfo);

  if (currentStatus.roomTemperature != previousStatus.roomTemperature)
  {
    heatpumpNode.setProperty("roomTemperature").send(String(currentStatus.roomTemperature));
    sendDebug(Debug.INFO, "  Room Temperature: " + String(currentStatus.roomTemperature));
  }

  if (currentStatus.operating != previousStatus.operating)
  {
    heatpumpNode.setProperty("operating").send(currentStatus.operating ? "True" : "False");
    sendDebug(Debug.INFO, "  Operating: " + currentStatus.operating);
  }

  // send the timer info
  const size_t bufferSizeTimers = JSON_OBJECT_SIZE(5);
  DynamicJsonBuffer jsonBufferTimers(bufferSizeTimers);

  JsonObject& rootTimers = jsonBufferTimers.createObject();
  rootTimers["mode"]          = currentStatus.timers.mode;
  rootTimers["onMins"]        = currentStatus.timers.onMinutesSet;
  rootTimers["onRemainMins"]  = currentStatus.timers.onMinutesRemaining;
  rootTimers["offMins"]       = currentStatus.timers.offMinutesSet;
  rootTimers["offRemainMins"] = currentStatus.timers.offMinutesRemaining;

  char bufferTimers[512];
  rootTimers.printTo(bufferTimers, sizeof(bufferTimers));

  heatpumpNode.setProperty("timers").send(bufferTimers);
  sendDebug(Debug.INFO, bufferTimers);

  if (currentStatus.timers.mode != previousStatus.timers.mode)
  {
    heatpumpNode.setProperty("timerMode").send(currentStatus.timers.mode);
    sendDebug(Debug.INFO, "  Timer Mode: " + String(currentStatus.timers.mode));
  }

  if (currentStatus.timers.onMinutesSet != previousStatus.timers.onMinutesSet)
  {
    heatpumpNode.setProperty("timerOnMins").send(String(currentStatus.timers.onMinutesSet));
    sendDebug(Debug.INFO, "  Timer On Mins: " + String(currentStatus.timers.onMinutesSet));
  }

  if (currentStatus.timers.onMinutesRemaining != previousStatus.timers.onMinutesRemaining)
  {
    heatpumpNode.setProperty("timerOnMinsRemaining").send(String(currentStatus.timers.onMinutesRemaining));
    sendDebug(Debug.INFO, "  Timer On Mins Remaining: " + String(currentStatus.timers.onMinutesRemaining));
  }

  if (currentStatus.timers.offMinutesSet != previousStatus.timers.offMinutesSet)
  {
    heatpumpNode.setProperty("timerOffMins").send(String(currentStatus.timers.offMinutesSet));
    sendDebug(Debug.INFO, "  Timer Off Mins: " + String(currentStatus.timers.offMinutesSet));
  }

  if (currentStatus.timers.offMinutesRemaining != previousStatus.timers.offMinutesRemaining)
  {
    heatpumpNode.setProperty("timerOffMinsRemaining").send(String(currentStatus.timers.offMinutesRemaining));
    sendDebug(Debug.INFO, "  Timer Off Mins Remaining: " + String(currentStatus.timers.offMinutesRemaining));
  }

  previousStatus = currentStatus;
}

void hpPacketDebug(byte* packet, unsigned int length, char* packetDirection)
{
  /*
  if (_debugMode)
  {
    String message;
    for (int idx = 0; idx < length; idx++)
    {
      if (packet[idx] < 16)
      {
        message += "0"; // pad single hex digits with a 0
      }
      message += String(packet[idx], HEX) + " ";
    }

    const size_t bufferSize = JSON_OBJECT_SIZE(1);
    DynamicJsonBuffer jsonBuffer(bufferSize);
    JsonObject& root = jsonBuffer.createObject();
    root[packetDirection] = message;

    char buffer[512];
    root.printTo(buffer, sizeof(buffer));

    if(!mqtt_client.publish(heatpump_debug_topic, buffer))
    {
      mqtt_client.publish(heatpump_debug_topic, "failed to publish to heatpump/debug topic");
    }
  }
  */
}
