# heatpump-monitor
*Monitoring and control of a Mistubishi heatpump via Homie over MQTT*

[![Build status](https://gitlab.com/philipbarclay/heatpump-monitor/badges/master/build.svg)](https://gitlab.com/philipbarclay/heatpump-monitor/commits/master)
[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

---
Monitoring ~~and control~~ of a Mitsubishi heatpump via the inbuilt CN105 serial connector.  Heatpump status transmitted via MQTT messages, later control via MQTT messages might be added.

This project is heavily inspired from the awesome work of Hadley Rich, author of the Python based MQMitsi running on a RasperryPi.  To support Hadleys work, I highly recommend his store [NiceGear](https://nicegear.co.nz) for any electronics purchases, especially from within New Zealand.

*   MQMitsi description: [Hacking a Mitsubishi Heap Pump / Air Conditioner](https://nicegear.co.nz/blog/hacking-a-mitsubishi-heat-pump-air-conditioner/)
*   MQMitsi code:  <https://github.com/hadleyrich/MQMitsi>

The original work from Hadley was used to create an Arduino library [HeatPump](https://github.com/SwiCago/HeatPump) able to be used on a variety of devices including the ESP8266 used here.

MQTT communications is based on the [Homie](https://git.io/homieiot) convention, implemented on the ESP8266 using the library [homie-esp8266](https://homie-esp8266.readme.io/)

## Hardware

Monitoring hardware is built around a [WeMos D1 module](https://wiki.wemos.cc/products:d1:d1_mini) featuring an ESP8266 processor with the inbuilt WiFi used to connect to a MQTT broker.  Interfacing to the heatpump is via a serial connection via a level shifter.  Power is extracted from the heatpump via the 12V supply rail and converted to 5V for powering the WeMoS module.

## Interfacing

The serial connection to the heatpump uses the Serial0 hardware module of the esp, but the IO is [swapped to the alternative pins](http://esp8266.github.io/Arduino/versions/2.1.0-rc2/doc/reference.html#serial) using `Serial.swap()`.  Thus GPIO15 is TX, GPIO13 is RX.

WeMoS IO|Function|ESP-8266 Pin|Usage 
---|---|---|---
TX | TXD | TXD | -
RX | RXD | RXD | -
A0 | Analog input, <3.3V | A0 | Unconnected, used for internal VCC measuring
D0 | IO | GPIO16 | -
D1 | IO, SCL | GPIO5 | I2C bus for ambient sensor 
D2 | IO, SDA | GPIO4 | I2C bus for ambient sensor
D3 | IO, 10k Pull-up | GPIO0 | - 
D4 | IO, 10k Pull-up, BUILTIN_LED | GPIO2 | On-board LED
D5 | IO, SCK | GPIO14 | 1-wire bus for exhaust temperature
D6 | IO, MISO | GPIO12 | -
D7 | IO, MOSI | GPIO13 | Serial RX from heatpump
D8 | IO, 10k Pull-down, SS | GPIO15 | Serial TX to heatpump
G | Ground | GND | Ground from heatpump
5V | 5V | - | Supply in, 12V from heatpump regulated to 5V
3V3 | 3.3V | 3.3V | -
RST | Reset | RST | -

## Software

HeatPump

homie-esp8266
